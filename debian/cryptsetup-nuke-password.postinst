#!/bin/sh

set -e

. /usr/share/debconf/confmodule

password_hash_path="/etc/cryptsetup-nuke-password/password_hash"

get_nuke_password() {
    db_get cryptsetup-nuke-password/password || true
    local NUKE_PASS="$RET"
    db_get cryptsetup-nuke-password/password-again || true
    local NUKE_PASS_CONFIRMATION="$RET"

    if [ "$NUKE_PASS" != "$NUKE_PASS_CONFIRMATION" ]; then
	return
    fi

    echo -n "$NUKE_PASS"
}

log() {
    if [ -n "$DEBCONF_RECONFIGURE" ]; then
	echo "$1"
    fi
}

store_password_hash() {
    local password=$(get_nuke_password)

    if [ -z "$password" ]; then
	if [ -e "$password_hash_path" ]; then
	    log "INFO: Keeping current nuke password."
	else
	    log "INFO: No nuke password found in debconf's database, nothing to configure."
	    log "INFO: Try 'dpkg-reconfigure cryptsetup-nuke-password' to set a nuke password."
	fi
	return
    fi

    echo "INFO: Storing the nuke password's crypted hash in $password_hash_path"
    mkdir -p $(dirname $password_hash_path)
    :> $password_hash_path
    chmod 600 $password_hash_path
    echo "$password" | /usr/lib/cryptsetup-nuke-password/crypt --generate >$password_hash_path

    # Drop the password from the debconf database for extra safety
    db_reset cryptsetup-nuke-password/password || true
    db_reset cryptsetup-nuke-password/password-again || true
}

configure_nuke_password() {
    db_get cryptsetup-nuke-password/already-configured || true
    what="$RET"

    case "$what" in
	keep)
	    # Nothing to do, move on
	    if [ -e "$password_hash_path" ]; then
		log "INFO: Keeping current nuke password."
	    fi
	    ;;
	remove)
	    if [ -e "$password_hash_path" ]; then
		echo "INFO: Removing current nuke password."
		rm -f "$password_hash_path"
	    fi
	    ;;
	overwrite)
	    store_password_hash
	    ;;
	*)
	    echo "WARNING: unexpected value in debconf's cryptsetup-nuke-password/already-configured: '$what'" >&2
	    ;;
    esac

    # Reset to default value for next time we reconfigure
    db_reset cryptsetup-nuke-password/already-configured || true
}

case "$1" in
    configure)
	configure_nuke_password
	;;
esac

#DEBHELPER#
